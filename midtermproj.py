#!/usr/bin/env python

import sys
import rospy
import moveit_commander
import moveit_msgs.msg
from geometry_msgs.msg import Pose

try:
    from math import pi, tau, dist, fabs, cos
except:
    from math import pi, fabs, cos, sqrt

#Defines tau value for below function to use, this is part of the tutorial code.
    tau = 2.0 * pi

#This function moves the arm to a better starting position to avoid singularity
def go_to_joint_state():
    moveit_commander.roscpp_initialize(sys.argv)
    robot = moveit_commander.RobotCommander()
    rospy.init_node('ur5e_writer', anonymous=True)
    group = moveit_commander.MoveGroupCommander("manipulator")
    
    #Set Specific joint angles for each of the 6 joints.
    joint_goal = group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -tau / 8
    joint_goal[2] = 0
    joint_goal[3] = -tau / 4
    joint_goal[4] = 0
    joint_goal[5] = tau / 6 
    

    group.set_joint_value_target(joint_goal)

    group.go(wait=True)
    # Calling ``stop()`` ensures that there is no residual movement
    group.stop()




#Apply pose position method to move the end effector
def move_ur5e_to_pose(pose_goal):
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('ur5e_writer', anonymous=True)
    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()
    group = moveit_commander.MoveGroupCommander("manipulator")
    group.set_pose_target(pose_goal)
    plan = group.go(wait=True)
    rospy.sleep(1)

if __name__ == '__main__':
    try:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('ur5e_writer', anonymous=True)
        robot = moveit_commander.RobotCommander()
        group = moveit_commander.MoveGroupCommander("manipulator")
        
        # initial position is set so EE starts from this postion everytime.
        # This is also the left Lower Corner of "B"
        b_posei = Pose()
        b_posei.position.x = 0.3
        b_posei.position.y = 0.1
        b_posei.position.z = 0.5
        b_posei.orientation.x = 0.0
        b_posei.orientation.y = 1.0
        b_posei.orientation.z = 0.0
        b_posei.orientation.w = 0.0

        # left Upper Corner of "B"
        b_pose1 = Pose()
        b_pose1.position.x = 0.3
        b_pose1.position.y = 0.5
        b_pose1.position.z = 0.5
        b_pose1.orientation.x = 0.0
        b_pose1.orientation.y = 1.0
        b_pose1.orientation.z = 0.0
        b_pose1.orientation.w = 0.0
       

        # Draw half of the upper semi-circle of "B"
        b_pose2 = Pose()
        b_pose2.position.x = 0.4
        b_pose2.position.y = 0.4
        b_pose2.position.z = 0.5  # Adjust the height of the top point
        b_pose2.orientation.x = 0.0
        b_pose2.orientation.y = 1.0
        b_pose2.orientation.z = 0.0
        b_pose2.orientation.w = 0.0
        

        # Draw entire of the upper semi-circle of "B"
        b_pose3 = Pose()
        b_pose3.position.x = 0.3
        b_pose3.position.y = 0.3
        b_pose3.position.z = 0.5
        b_pose3.orientation.x = 0.0
        b_pose3.orientation.y = 1.0
        b_pose3.orientation.z = 0.0
        b_pose3.orientation.w = 0.0
    

        # Draw half of the lower semi-circle of "B"
        b_pose4 = Pose()
        b_pose4.position.x = 0.4
        b_pose4.position.y = 0.2
        b_pose4.position.z = 0.5
        b_pose4.orientation.x = 0.0
        b_pose4.orientation.y = 1.0
        b_pose4.orientation.z = 0.0
        b_pose4.orientation.w = 0.0

         # "B" finished
        b_pose5 = Pose()
        b_pose5.position.x = 0.3
        b_pose5.position.y = 0.1
        b_pose5.position.z = 0.5
        b_pose5.orientation.x = 0.0
        b_pose5.orientation.y = 1.0
        b_pose5.orientation.z = 0.0
        b_pose5.orientation.w = 0.0

        # The lower starting point of "C"
        c_pose1 = Pose()
        c_pose1.position.x = 0.3
        c_pose1.position.y = 0.1
        c_pose1.position.z = 0.5
        c_pose1.orientation.x = 0.0
        c_pose1.orientation.y = 1.0
        c_pose1.orientation.z = 0.0
        c_pose1.orientation.w = 0.0

        # Draw half of "C"
        c_pose2 = Pose()
        c_pose2.position.x = 0.1
        c_pose2.position.y = 0.3
        c_pose2.position.z = 0.5
        c_pose2.orientation.x = 0.0
        c_pose2.orientation.y = 1.0
        c_pose2.orientation.z = 0.0
        c_pose2.orientation.w = 0.0
        
        # "C" finished 
        c_pose3 = Pose()
        c_pose3.position.x = 0.3
        c_pose3.position.y = 0.5
        c_pose3.position.z = 0.5
        c_pose3.orientation.x = 0.0
        c_pose3.orientation.y = 1.0
        c_pose3.orientation.z = 0.0
        c_pose3.orientation.w = 0.0

       
        # Check on the Joint state
        go_to_joint_state()

        # Execute "B" and "C" path
        move_ur5e_to_pose(b_posei)
        move_ur5e_to_pose(b_pose1)
        move_ur5e_to_pose(b_pose2)
        move_ur5e_to_pose(b_pose3)
        move_ur5e_to_pose(b_pose4)
        move_ur5e_to_pose(b_pose5)
        move_ur5e_to_pose(c_pose1)
        move_ur5e_to_pose(c_pose2)
        move_ur5e_to_pose(c_pose3)



    except rospy.ROSInterruptException:
        pass