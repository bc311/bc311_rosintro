#!/usr/bin/bash

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[5.0,0.0,0.0]' '[0.0,0.0,3.14]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0,7.0,0.0]' '[0.0,0.0,0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0,0.0,5.0]' '[0.0,0.0,3.14]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[5.0,0.0,0.0]' '[0.0,0.0,3.14]'

rosservice call /spawn 10 5 0 ""



rosservice call /turtle2/set_pen '255' '0' '0' '1' 'off'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.0,0.0,5.0]' '[0.0,0.0,3.14]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[5.0,0.0,0.0]' '[0.0,0.0,3.14]'
